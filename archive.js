/** holen der datenquelle */
var db = API.getDatabaseForObject("Anzeige");
/** holen aller aktiven anzeigedokumente */
var col = db.createNoteCollection(false);
col.setSelectDocuments(true);
col.setSelectionFormula('form="Anzeige" & archive!="1"');
col.buildCollection();

if(col.getCount() > 0){
    /** loop durch alle noteids */
    var noteId = col.getFirstNoteID();
    while(noteId.length() > 0){
        /** dokument anhand der noteid holen */
        var doc = db.getDocumentByID(noteId);
        /** java date */
        var lastmod = doc.getLastModified().toJavaDate();
        /** jetzt-zeit ermitteln */
        var now = new java.util.Date();
        /** prüfen auf lastModified + n tage < today */
        var calModified = java.util.Calendar.getInstance();
        calModified.setTime(lastmod);
        /** n monate dazu */
        var n = 3;
        calModified.add(calModified.MONTH, n);
        lastMod = calModified.getTime();

        if(lastmod.before(now)){
            /** flag setzen */
            doc.replaceItemValue("archive", "1");
            doc.save();
        }

        noteId = col.getNextNodeID(noteId);
    }
}
