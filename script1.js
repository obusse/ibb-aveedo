/** JS introduction */

/* java script comment */

/** case sensitiv */
var x = 0;
var X = 1;
var y = x != X; // returns true

/** classnames = Capital */
/** method names = camel case */

// Obj.getFirstName();

/** semicolon */
var foo = "";
var bar = "";

for (var x = 0; x < 10; x++) {
    /** */
}

/** declarations */

var foo = 10;

alert(foo); // -> 10.0

alert(parseInt(foo)); // -> 10

/** javascript object notation JSON */

var Obj = {
    "name": "Busse",
    "firstname": "Oliver"
};

alert(Obj.name); // -> "Busse"

/** REST services !!! */
/** jsononlineeditor.org */

/** --------------------------------------------- */

/** functions */

var global = "";

function foo(param) {
    var local = 123;
    /** mach was */
    alert(param);
    global = "456";
}

alert(local); // -> undefined
alert(global); // -> abc
foo("");
alert(global); // -> 456

foo("test");

/** JSON function */

var MyClass = {
    status: true,
    executeStuff: function (param) {
        MyClass.status = false;
    },
    foobar2: function (param1, param2) {
        MyClass.status = true;
    }
}


/** call */

MyClass.foobar2("", "");

/** callbacks */

var Obj2 = {
    main: function (param, callback, error) {
        /** ... */
        try {
            if (param) {
                callback();
            }
        } catch (e) {
            if (error)
                error(e);
            console.log(e);
        }
        /**... */
    }
}

Obj2.main(true, function () {
    alert("foo");
});

Obj2.main(false, function () {
    alert("bar");
}, function(error){
    alert(error);
});